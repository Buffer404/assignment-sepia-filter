#include "../include/formats/bmp.h"
#include "../include/transform/sepia.h"
#include "../include/utils/util.h"
#include <stdio.h>
#include <stdlib.h>

void clean_output(struct image *outputImage) {
    free(outputImage->data);
    free(outputImage);
}

void clean_input(struct image *inputImage) {
    free(inputImage->data);
    free(inputImage);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Введите путь до входящего и исходящего файлов\n");
        exit(1);
    }

    FILE *inputFile = NULL;
    FILE *outputFile = NULL;
    struct image *inputImage = (struct image *)malloc(sizeof(struct image));
    struct image *outputImage = NULL;

    enum open_status openStatus = open_file(&inputFile, argv[1], "rb");

    enum read_status readStatus = from_bmp(inputFile, inputImage);

    int close_status = close_file(inputFile);

    outputImage = sepia_filter(inputImage);

    openStatus = open_file(&outputFile, argv[2], "wb");

    enum write_status writeStatus = to_bmp(outputFile, outputImage);

    close_status = close_file(outputFile);


    clean_output(outputImage);
    clean_input(inputImage);

    return 0;
}
