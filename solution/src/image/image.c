#include <stdlib.h>
#include <inttypes.h>
#include "../../include/image/image.h"



struct image* create(uint64_t width, uint64_t height) {
    struct image *img = malloc(sizeof(struct image));
    img->width = width;
    img->height = height;
    img->data =
            (struct pixel *) malloc(sizeof(struct pixel) * width * height);
    return img;
}

void deinitialize(struct image* img){
    free(img->data);
    free(img);
}

