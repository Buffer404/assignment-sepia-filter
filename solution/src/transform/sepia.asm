global sepia
section .text

sepia:
        push    r12
        push    rbp
        push    rbx
        mov     rbx, QWORD PTR [rdi+8]
        mov     r11, QWORD PTR [rdi]
        test    rbx, rbx
        je      .L1
        test    r11, r11
        je      .L1
        mov     r10, rdi
        lea     rbp, [r11+r11*2]
        xor     r8d, r8d
        xor     r9d, r9d
        movsd   xmm4, QWORD PTR .LC0[rip]
        movsd   xmm3, QWORD PTR .LC1[rip]
        movsd   xmm2, QWORD PTR .LC2[rip]
.L7:
        mov     rdi, QWORD PTR [r10+16]
        mov     rsi, QWORD PTR [r11+16]
        lea     rcx, [rdi+r8]
        add     rsi, r8
        add     r8, rbp
        add     rdi, r8
.L6:
        movzx   eax, BYTE PTR [rcx+1]
        pxor    xmm0, xmm0
        pxor    xmm1, xmm1
        mov     edx, -50
        cvtsi2sd        xmm0, eax
        movzx   eax, BYTE PTR [rcx+2]
        cvtsi2sd        xmm1, eax
        movzx   eax, BYTE PTR [rcx]
        mulsd   xmm0, xmm4
        mulsd   xmm1, xmm3
        addsd   xmm0, xmm1
        pxor    xmm1, xmm1
        cvtsi2sd        xmm1, eax
        mulsd   xmm1, xmm2
        addsd   xmm0, xmm1
        cvttsd2si       eax, xmm0
        cmp     al, dl
        cmovbe  edx, eax
        lea     r12d, [rdx+49]
        mov     edx, 14
        cmp     al, dl
        mov     BYTE PTR [rsi], r12b
        cmovnb  edx, eax
        sub     edx, 14
        mov     BYTE PTR [rsi+1], dl
        cmp     al, 55
        jbe     .L3
        sub     eax, 46
        add     rcx, 3
        add     rsi, 3
        mov     BYTE PTR [rsi-1], al
        cmp     rcx, rdi
        jne     .L6
        add     r9, 1
        cmp     rbx, r9
        jne     .L7
.L1:
        pop     rbx
        mov     rax, r11
        pop     rbp
        pop     r12
        ret
.L3:
        add     rcx, 3
        mov     BYTE PTR [rsi+2], 0
        add     rsi, 3
        cmp     rcx, rdi
        jne     .L6
        add     r9, 1
        cmp     rbx, r9
        jne     .L7
        jmp     .L1
.LC0:
        .long   962072674
        .long   1071827124
.LC1:
        .long   -446676599
        .long   1070801616
.LC2:
        .long   -1614907703
        .long   1069362970