#include "formats/image.h"
#include <inttypes.h>
#include <stdlib.h>

struct image *sepia(struct image const *input_image);


struct image *sepia_filter(struct image const *input_image) {

// commented C code, refactored to ASM and used ASM function (that was expected?)

//    struct image *result_image =
//            image_init(input_image->width, input_image->height);
//    uint8_t tone;
//    struct pixel pxl;
//    for(size_t y = 0; y < input_image->height; y++) {
//        for (size_t x = 0; x < input_image->width; x++) {
//            pxl = input_image->data[input_image->width*y +x];
//            tone = (uint8_t)(pxl.r*0.299 + pxl.g*0.587 +pxl.b*0.114);
//            pxl.b = (uint8_t)((tone > 206) ? 255 : tone + 49);
//            pxl.g = (uint8_t)((tone < 14) ? 0 : tone - 14);
//            pxl.r = (uint8_t)((tone < 56) ? 0 : tone - 46);
//            result_image->data[input_image->width*y +x] =
//                    pxl;
//        }
//    }
//
//    return image_status_init(READ_OK, "Sepia successfully.", result_image);

    return sepia(input_image);

}
