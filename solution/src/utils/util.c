#include <stdlib.h>
#include <stdio.h>
#include "../../include/utils/util.h"

enum open_status open_file(FILE** file, const char *path,
                           const char *modes) {
    *file = fopen(path, modes);
    if (*file == NULL) {
        return OPEN_FAILED;
    }
    return OPEN_OK;
}

int close_file(FILE *file) { return fclose(file); }
