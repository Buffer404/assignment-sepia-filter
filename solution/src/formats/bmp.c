#include <stdio.h>
#include "../../include/image/image.h"


enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );
